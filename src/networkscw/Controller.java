package networkscw;

import java.util.ArrayList;

/*
 * TextDuplex.java
 *
 * Created on 15 January 2003, 17:11
 */

/**
 *
 * @author  abj
 */
public class Controller {    
    
    // Controller Variables (Static)
    static String receiverIP = "localhost";
    static boolean running = true;    
    
    // Audio Variables (Static)
    static ArrayList<byte[]> recordList = new ArrayList<>();
    static ArrayList<byte[]> playList = new ArrayList<>();
    static int dimension = 3;
    static int dimensionSqr = (dimension*dimension);
    
    public static void main (String[] args){
                
        Recorder recorder = new Recorder();
        Sender sender = new Sender();
        Receiver receiver = new Receiver();
        Player player = new Player();
        
        //player.start();
        receiver.start(); 
        recorder.start();
        sender.start();
        
        
        System.out.println("System is Online. BOOOOOOOOOOOOOOOOOOOOOOM");
    }            
}