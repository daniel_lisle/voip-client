/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package networkscw;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.LineUnavailableException;

/**
 *
 * @author sec10cgu
 */
public class Player implements Runnable {

    public void start() {
        Thread thread = new Thread(this);
        thread.start();
    }

    public void run() {

        // Create Player
        CMPC3M06.AudioPlayer player = null;
        try {
            player = new CMPC3M06.AudioPlayer();
        } catch (LineUnavailableException ex) {
            Logger.getLogger(Receiver.class.getName()).log(Level.SEVERE, null, ex);
        }

        while (Controller.running) {

            try {
                // Plays Block of sound from dList   
                if (!Controller.playList.isEmpty()) {
                    byte[] playBack = new byte[512];
                    System.arraycopy(Controller.playList.remove(Controller.playList.size() - 1), 2, playBack, 0, 512);
                    player.playBlock(playBack);
                }
            } catch (Exception e) {
                System.out.println("ERROR: AudioReceiver: Some random IO error occured - Player!");
                e.printStackTrace();
            }
        }

        // Close Player
        player.close();
    }
}
