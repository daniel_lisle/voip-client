/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package networkscw;

import java.net.DatagramPacket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Random;

// Datagram Sockets
import java.net.DatagramSocket;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.LineUnavailableException;
import uk.ac.uea.cmp.voip.DatagramSocket2;
import uk.ac.uea.cmp.voip.DatagramSocket3;

/**
 *
 * @author fzn10fqu
 */
public class Receiver implements Runnable {

    static DatagramSocket receiving_socket;

    public void start() {
        Thread thread = new Thread(this);
        thread.start();
    }

    public void run() {

        CMPC3M06.AudioPlayer player = null;
        try {
            player = new CMPC3M06.AudioPlayer();
        } catch (LineUnavailableException ex) {
            Logger.getLogger(Receiver.class.getName()).log(Level.SEVERE, null, ex);
        }

        LinkedList<byte[]> receiveList = new LinkedList<>();
        ArrayList<byte[]> blockList = new ArrayList<>();
        ArrayList<byte[]> toDealWith = new ArrayList<>();
        boolean initiate = false;
        byte[] noise = new byte[514];

        Random randomGenerator = new Random();
        for (int i = 0; i < noise.length; i++) {
            noise[i] = (byte) randomGenerator.nextInt(255);
        }

        // Set port
        int PORT = 8000;

        //***************************************************
        //Open a socket to receive from on port PORT
        //DatagramSocket receiving_socket;
        try {
            receiving_socket = new DatagramSocket3(PORT);
        } catch (SocketException e) {
            System.out.println("ERROR: AudioReceiver: Could not open UDP socket to receive from.");
            e.printStackTrace();
            System.exit(0);
        }
        //***************************************************

        //***************************************************
        //Main loop.

        // Deinterleve Variables
        int currentPacket = 0;
        int blockSize = 0;
        int currentBlock = 0;

        // Setting Timeout
        try {
            receiving_socket.setSoTimeout(1000);
        } catch (SocketException ex) {
            System.out.println("ERROR: AudioReceiver: Socket Error!");
            ex.printStackTrace();
        }

        while (Controller.running) {

            try {

                // Receive Packet
                byte[] buffer = new byte[514];
                DatagramPacket packet = new DatagramPacket(buffer, 0, buffer.length);
                receiving_socket.receive(packet);
                receiveList.add(buffer);

                // Acquire blockSize
                if (!initiate) {

                    blockSize = receiveList.get(0)[0];
                    initiate = true;
                }

                // Go through 
                if (receiveList.size() >= blockSize * 2) {

                    // Initiate ArrayList to blockSize
                    for (int i = 0; i < blockSize; i++) {
                        blockList.add(null);
                    }

                    if (currentPacket >= (255 - (255 % blockSize))) {
                        currentBlock = 0;
                        currentPacket = 0;
                        System.out.println("Reset");
                    }

                    System.out.println("Receive List " + receiveList.size());

                    for (int i = 0; i < blockSize * 2; i++) {
                        System.out.println("Packet to be checked: " + (receiveList.get(0)[1] & 0xFF));
                        if ((receiveList.get(0)[1] & 0xFF) >= (currentBlock * blockSize)
                                && (receiveList.get(0)[1] & 0xFF) < (blockSize * (currentBlock + 1))) {
                            System.out.println("Cal 1 : " + ((blockSize * (currentBlock + 1) - 1) - (receiveList.get(0)[1] & 0xFF)));
                            blockList.set(((blockSize * (currentBlock + 1)) - 1) - (receiveList.get(0)[1] & 0xFF), receiveList.remove(0));

                        } else {
                            //If a packet is out of the range then it is added to a temp array so that the other packets can be checked
                            System.out.println("Packet out of range: " + (receiveList.get(0)[1] & 0xFF));
                            toDealWith.add(receiveList.remove(0));
                        }
                    }

                    for (int i = 0; i < blockList.size(); i++) {
                        if (blockList.get(i) == null) {

                            Controller.playList.add(new byte[514]);
                            currentPacket++;
                            System.out.println("Creating scilence");


                        } else {
                            Controller.playList.add(blockList.get(i));
                            System.out.println("Added Audio");
                            currentPacket++;
                        }
                    }

                    for (int i = 0; i < Controller.playList.size(); i++) {
                        byte[] playBack = new byte[512];
                        System.arraycopy(Controller.playList.remove(Controller.playList.size() - 1), 2, playBack, 0, 512);
                        player.playBlock(playBack);
                        System.out.println("Playlist: " + Controller.playList.size());
                    }

                    //Items that could not be delt with are added back the the front of the receive list
                    for (int i = 0; i < toDealWith.size(); i++) {
                        System.out.println("Adding back");
                        receiveList.addFirst(toDealWith.remove(toDealWith.size()-1));
                    }

                    currentBlock++;
                    blockList.clear();
                }
//                // Deinterleve 
//                if (receiveList.size() >= blockSize) {
//                    found = false;
//
//                    packetFound:
//                    if (!found) {
//          
//                        // Checks block
//                        for (int i = 0; i < blockSize; i++) {
//                            if ((receiveList.get(i)[1] & 0xFF) == currentPacket) {
//                                //System.out.println("Packet Found " + (receiveList.get(i)[1] & 0xFF));
//                                //byte[] playBack = new byte[512];
//                                //System.arraycopy(receiveList.remove(i), 2, playBack, 0, 512);
//                                //Controller.playList.add(playBack);
//                                Controller.playList.add(receiveList.remove(i));
//                                found = true;
//                                break packetFound;
//                            }
//                        }
//                    }
//
//                    //No packet was found so a method of compensation is used
//                    if (!found) {
//                        //If there a previous packet that can be repeated that is used
//                        if (Controller.playList.size() > 1) {
//                            //System.out.println("Repeating previous byte");
//                            Controller.playList.add(Controller.playList.get(Controller.playList.size() - 1));
//
//                            //Otherwise a byte of scilence is used to preserve timing
//                        } else {
//                            //System.out.println("No previous byte playing scilence");
//                            Controller.playList.add(new byte[512]);
//                        }
//                    }

                // Auto increment
                //currentPacket++;              

            } catch (Exception e) {
                System.out.println("ERROR: AudioReceiver: Some random IO error occured!");
                e.printStackTrace();
            }
        }
// Close receiving socket
        receiving_socket.close();
    }
}
