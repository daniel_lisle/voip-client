/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package networkscw;

import java.net.DatagramSocket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.LineUnavailableException;

/**
 *
 * @author fzn10fqu
 */
public class Recorder implements Runnable {

    static DatagramSocket sending_socket;

    public void start() {
        Thread thread = new Thread(this);
        thread.start();
    }

    public void run() {

        // Absolute Position of recording time (incrememts)
        int absolutePos = 0, absoluteMax = 0;        
        
        // Dimension of interleaving
        int dimension = Controller.dimension;
        
        // Block size to be sent
        int dimensionSqr = dimension * dimension;

        // Calculate Max
        absoluteMax = (255 - (255 % dimensionSqr))-1;

        //Initialise AudioPlayer and Recorder objects
        CMPC3M06.AudioRecorder recorder = null;
        try {
            recorder = new CMPC3M06.AudioRecorder();
        } catch (LineUnavailableException ex) {
            Logger.getLogger(Recorder.class.getName()).log(Level.SEVERE, null, ex);
        }

        while (Controller.running) {

            try {
                //Capture audio data and add to voiceVector                

                // Records dimensionSqr worth of blocks.
                for (int i = 0; i < dimensionSqr; i++) {
                    byte[] tempBuffer = recorder.getBlock();
                    byte[] buffer = new byte[514];
                    System.arraycopy(tempBuffer, 0, buffer, 2, 512);
                    buffer[0] = (byte) dimensionSqr;
                    buffer[1] = (byte) absolutePos;
                    Controller.recordList.add(buffer);
                    absolutePos++;

                    // Resets absolutePos to keep in bounds of single Byte
                    if (absolutePos > absoluteMax) {
                        absolutePos = 0;
                    }                   
                }               

            } catch (Exception e) {
                System.out.println(e);
                System.exit(0);
            }

        }        

        //Close audio input
        recorder.close();
    }
}
