/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package networkscw;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import static networkscw.Recorder.sending_socket;
import uk.ac.uea.cmp.voip.DatagramSocket2;
import uk.ac.uea.cmp.voip.DatagramSocket3;

/**
 *
 * @author sec10cgu
 */
public class Sender implements Runnable {

    public void start() {
        Thread thread = new Thread(this);
        thread.start();
    }

    public void run() {

        //***************************************************
        //Port to send to
        int PORT = 8000;
        //IP ADDRESS to send to
        InetAddress clientIP = null;
        try {
            clientIP = InetAddress.getByName("localhost");  //CHANGE localhost to IP or NAME of client machine
        } catch (UnknownHostException e) {
            System.out.println("ERROR: AudioSender: Could not find client IP");
            e.printStackTrace();
            System.exit(0);
        }
        //***************************************************
        //DatagramSocket sending_socket;
        try {
            sending_socket = new DatagramSocket3();
        } catch (SocketException e) {
            System.out.println("ERROR: AudioSender: Could not open UDP socket to send from.");
            e.printStackTrace();
            System.exit(0);
        }
        //***************************************************

        // Sender Variables
        int i = 0, j = 0;                               // Used by Interleaver
        int dimension = 0, dimensionSqr = 0;            // Square size
        int[] interLoc = null;                          // iniate empty
        ArrayList<byte[]> tempList = new ArrayList<>();
        ArrayList<byte[]> sendList = new ArrayList<>();

        // Controller
        boolean intCal = false;

        // Enters controlled loop
        while ((Controller.running)) {

            // Calculates initial values (Once only)
            if (intCal == false) {
                // Grab initial values from first recording
                if (!Controller.recordList.isEmpty()) {
                    byte[] tempByte = Controller.recordList.get(0);
                    dimension = Controller.dimension;
                    dimensionSqr = Controller.dimensionSqr;
                    interLoc = new int[dimensionSqr];

                    // Calculates block ordering positions by dimensionSqr size 
                    for (int x = 0; x < dimensionSqr; x++) {
                        // i = y axis, j = x axis -- jd + (d-1-i)
                        if ((x % dimension == 0) && (x != 0)) {
                            j = 0;
                            i++;
                        }
                        interLoc[x] = j * dimension + (dimension - 1 - i);
                        j++;
                    }

                    // Enables calculation once
                    intCal = true;
                }
            }

            //System.out.println(Controller.recordList.size());
            // Adds to sendList
            if ((Controller.recordList.size() >= dimensionSqr) && (intCal == true)) {

                // Remove taken block
                for (int k = 0; k < dimensionSqr; k++) {
                    tempList.add(Controller.recordList.remove(0));
                }

                // Add to bufferlist
                for (int k = 0; k < dimensionSqr; k++) {
                    
                    //System.out.println("K: " + k + " - Interloc: " + interLoc[k] + " - tempList: " + (tempList.get(interLoc[k])[1] & 0xFF));
                    sendList.add(tempList.get(interLoc[k]));
                }

                // Try Catch to handle random IO errors
                try {
                    // Sends as DatagramPacket
                    for (int p = 0; p < dimensionSqr; p++) {
                        byte[] buffer = sendList.remove(0);
                        DatagramPacket packet = new DatagramPacket(buffer, buffer.length, clientIP, PORT);
                        sending_socket.send(packet);
                    }
                } catch (Exception e) {
                    System.out.println("ERROR: AudioReceiver: Some random IO error occured!");
                    e.printStackTrace();
                }

                // Clear templist
                tempList.clear();
            }
        }

        // Close sending socket
        sending_socket.close();
    }
}